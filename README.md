# RequestInput.JS

## What is it and why should I use it?
Do you want to ask for CLI input?
Do you want to use the prompt js function but hate the alert box?

RI.JS (RequestInput.JS) might help!

RI.JS is a program that, when using a browser, uses MaterializeCSS and MaterializeJS to make a synchronous prompt for all your input needs, and when using the CLI, waits for input in the terminal.
Nothing revolutional, just something i made because i was bored and have had this issue before.
